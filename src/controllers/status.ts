import {Request, Response} from "express";
import * as got from 'got';
import "../lib/env";
import * as log from 'winston';


const getUrl = process.env.SAP_HANNA_ENDPOINT;

export let purchaseOrderListInfo = async (req: Request, res: Response) => {
    try {
        let responseDetails = await got.get(getUrl, {
            headers: {
                'Content-Type': 'application/json',
                'x-csrf-token': process.env.CSRF_TOKEN,
                'Accept': 'application/json',
                'apikey': process.env.API_KEY
            },
            json: true
        });
        log.debug('body ' + JSON.stringify(responseDetails.body));
        if (responseDetails.statusCode === 200) {
            res.send(responseDetails.body);
        } else if (!responseDetails) {
            log.warn('error in fetching api response');
            res.status(200);
            res.send('DATA_NOT_FOUND');
        }
    } catch (e) {
        log.warn(`error in processing ${e}`);
        res.status(401);
        res.send(`${e}`);
    }

};
