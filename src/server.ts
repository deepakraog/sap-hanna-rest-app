import app from "./app";
import * as log from 'winston';

const server = app.listen(app.get("port"), () => {
    log.info(`App is running on http://localhost:${ app.get('port')} in ${app.get('env')} mode`);
});

export default server;
