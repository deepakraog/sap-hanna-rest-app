import * as log from 'winston';

import express = require("express");
import { purchaseOrderListInfo } from "./controllers/status";

import "./lib/env";

// Our Express APP config
const app = express();
app.set("port", process.env.PORT || 3000);

// API Endpoints
app.get("/sap-hanna/po-items", async (req, res) => {
    log.info('inside /sap-hanna/po-items api call');
    await purchaseOrderListInfo(req, res);
});

// export our app
export default app;
